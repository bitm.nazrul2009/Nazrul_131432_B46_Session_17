<?php


interface canFly{
    public function fly();
   }

interface canSwim{
    public function swim();
}

class Bird{

    public function info (){

        echo "I'm $this->name!<br>I'm a Bird<br>";

    }
}


class Dove extends Bird implements canFly{
    public $name="Dove";
    public function fly()
    {
        echo "I can Fly....<br>";
    }

}

class Penguin extends Bird implements canSwim{
    public $name="Penguin";
    public function swim()
    {
        echo "I can Swim....<br>";
    }

}

class Duck extends Bird implements canSwim, canFly{
    public $name="Duck";
        public function swim()
    {
        echo "I can Swim....<br>";
    }

    public function fly()
    {
        echo "I can Fly....<br>";
    }


}


describe(new Dove());


function describe($bird){

    $bird->info();

    if($bird instanceof canFly){
        $bird->fly();
    }

    if ($bird instanceof canSwim);
        $bird->swim();


}