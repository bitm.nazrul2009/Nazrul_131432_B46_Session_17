<?php


abstract class MyAbstractClass{

    abstract protected function forceMethod1();
    abstract public function forceMethod2();

    public $a, $b, $c;

    public function normalMethod(){

        echo "inside ".__METHOD__."<br>";

    }

}

class MyClass extends MyAbstractClass{

    public function forceMethod1()
    {
        echo "inside ".__METHOD__."<br>";
    }

    public function forceMethod2()
    {
        echo "inside ".__METHOD__."<br>";
    }

}

$obj = new MyClass();

$obj->forceMethod1();
$obj->forceMethod2();