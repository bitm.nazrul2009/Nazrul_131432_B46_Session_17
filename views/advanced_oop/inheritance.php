<?php


trait A{
    public $a;

    public function doSomething(){
        echo "doing something inside ".__TRAIT__."<br>";
    }
}


trait B{
    public $b;

    public function doSomething(){
        echo "doing something inside ".__TRAIT__."<br>";
    }
}

class AB{
    use A, B {

        A::doSomething insteadof B;
    }

}

class BA{
    use A, B {

        B::doSomething insteadof A;
    }
}


$objAB = new AB();
$objAB->doSomething();

$objBA = new BA();
$objBA->doSomething();


class Both{

    use A,B{
        A::doSomething insteadof B;
        B::doSomething As doAnything;
    }
}

$obj = new Both;

$obj->doSomething();
$obj->doAnything();
